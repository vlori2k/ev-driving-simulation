import sys
def calculate_percent(all,part):
    return  ((all+0.0) / (part+0.0) * 100.0)

def calculate_percent_list(all,part):
    results = []
    for i in range(len(all)):
        results.append(100 - calculate_percent(all[i],part[i]))
    return results

def create_list_of_percentages(all, part):
    list_of_percentages = []
    for i in range(len(all)):
        list_of_percentages.append(calculate_percent_list( all[i], part[i]))
    return list_of_percentages

def make_dictionary_percent(distances, range_dictionary):
    percentage_dictionary = {}
    for d in range_dictionary:
        percentage_dictionary[d] =  create_list_of_percentages(distances, range_dictionary[d])

        for e in percentage_dictionary[d]:
            print("\t" + str(e) )

    return percentage_dictionary

def calculate_percent2(all,part):
    return  ((part+0.0) / (all+0.0) *100.0)

def calculate_percent3(all,part):
    return  ((part+0.0) / (all+0.0) *100.0)

def calculate_percent4(all,part):
    return  ((part+0.0) / (all+0.0) *100.0)

def calculate_remaining_SOC(route=None, model=None, data=None, EVs=None, ranges=None, districts=None, abortOn=0.0, distance_already_travelled=0, override_start_percent=None):
    print(" ---------------------------- Begin route ---------------------------- ")
    print('')
    print(route)
    print("You pass through", (len(route)), "districts during this trip")
    print(" --------------------------------------------------------------------- ")
    print('')

    To = route[0]
    total_distance = distance_already_travelled
    model_index = EVs.index(model)

    Range = float( ranges[ model_index ] )

    remaining_range = Range - total_distance
    soc_status = calculate_percent2(Range,
                                   remaining_range)
    Comparison_EV =[ soc_status ]

    if override_start_percent:
        Comparison_EV = [override_start_percent]

        total_distance = (override_start_percent / 100.0) * Range

        print("Override start range: " + str(Range) + " -> " + str(total_distance))

    print("Start percent: " + str(Comparison_EV) )

    for i in range(1,len(route)):
        From = To
        To = route[i]
        print("From " + From + " to " + To)

        cost_distance = data[districts.index(From)][districts.index(To)]
        total_distance = total_distance + cost_distance

        remaining_range = Range - total_distance
        soc_status = calculate_percent2(Range, remaining_range)
        Comparison_EV.append(soc_status)

        if   soc_status < 60:   print("\t(60%), everything is fine ")
        elif soc_status < 50:print("\t(50%), everything is fine ")

        elif soc_status < 40:
            print("\tLOW (40%), charge if possible ")

        elif soc_status < 20:
            print("\tCRITCAL LOW! Go to the nearest charging station!")

        if soc_status < abortOn:
            print("Abort trip")
            Comparison_EV.append("-Abort-")
            return Comparison_EV

    remaining_range = Range - total_distance
    soc_status = calculate_percent2(Range, remaining_range)

    print(" -------------------------Route Complete---------------------------- ")
    print('')
    print("Range: "   + str(Range))
    print("Dist: "    + str(total_distance))
    print("Remaining range left: " + str(remaining_range)[:4] + " km")
    print("Remaining battery status:" + "\t" + str(soc_status)[:3] + "%SOC")

    return Comparison_EV



