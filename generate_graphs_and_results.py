from calculate_battery_status import *
import matplotlib.pyplot as plt
import random

def get_random_index(list):
    if not list:
        print("Error getRandomIndex: list is empty")
        return None
    r = random.randint(0, len(list) - 1)
    return r

def plot_result_to_a_graph(r, all_labels=None, overRideNameList=[], distances=[]):
    plotting_window = plt.figure()
    subplot = plotting_window.add_subplot(111)

    sidx = 0
    for y_labels in r:
        name = ""
        if all_labels != None:
            name = all_labels[r.index(y_labels)]
        if overRideNameList != []:
            name = overRideNameList[sidx]
            sidx += 1


        x_labels = [x for x in range(0, len(y_labels))]

        plt.xticks(x_labels, distances)


        tmpX = 0
        for el in y_labels:
            print(x_labels)

            subplot.annotate( str(el)[0:5],xy=(tmpX,el-1.0 ) )
            tmpX += 1

        subplot.plot(x_labels, y_labels, label=name)

    if all_labels != None: plt.legend()

    plt.show()



