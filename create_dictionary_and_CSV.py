from distance_matrixes import *
from top_10_EVs_norway import *
import random

def create_dictionary_of_ranges(EVs, ranges, verbose=False):
    dictionary_of_ranges = {}
    for EV in range(len(EVs)):
        model = EVs[EV]
        max_distances = range_in_KM_norwegian_winter[EV]
        remaining_ranges = []

        for row in distances:
            for element in row:
                 new_row_of_remaining_ranges = [max_distances-element for element in row]
            remaining_ranges.append(new_row_of_remaining_ranges)
        dictionary_of_ranges[ model ] = remaining_ranges

    for EVmodel in dictionary_of_ranges:
        print(EVmodel)

        for row in dictionary_of_ranges[EVmodel]:
            print("     " + str(row))

    return dictionary_of_ranges

def write_a_CSV_file(name, distances, header, path="Remaining_Ranges_CSV"):
    fileName = path + "/" + name + ".csv"

    print("MAKING CSV " + str(name) )

    with open(fileName, "w") as file:

        for by in header[:-1]:
            file.write(str(by))
            file.write(';')

        file.write(str(header[-1]))
        file.write( "\n" )

        for row in distances:
            for e in row[:-1]:
                file.write( str(e) )
                file.write(';')
            file.write( str( row[-1]) )
            file.write("\n")

def get_a_random_index(lis):
    if not lis:
        print("Error getRandomIndex: list is empty")
        return None
    random_indexes = random.randint(0,len(lis)-1)
    return random_indexes