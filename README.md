# EV-driving-simulation

- Different EVs drives on different points through a path using distance matrix
- When simulation is finished, 2 types of CSV files are generated for each EV model:
  1. CSV file that shows remaining ranges left from a point on the path
  2. CSV file that shows remaining SoC% left from a point on the path
- Also, graphs is plotted that shows remaining SoC% left for each EV
- User can also set own "start-distance", e,g: 50km consumed before simulation 


