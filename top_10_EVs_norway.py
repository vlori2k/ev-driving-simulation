import numpy as np

EV_models =                                  ['Nissan Leaf 24 kWh pack', 'Nissan Leaf 30 kWh pack', 'Volkswagen e-Golf', 'Tesla Model S 90(D)', 'BMW i3 22kWh', 'BMW i3 33 kWh', 'KIA SOUL', 'Volkswagen e-Up(2016)', 'Renault ZOE R90 Z.E 40', 'Hyundai IONIQ', 'Mercedes-Benz B250E', 'Tesla model X 90D']
range_in_KM_by_manufacturer = np.array([           199, 250, 300, 557, 190, 300, 250, 160, 403, 280, 200, 489])
range_in_KM_norwegian_summer = np.array([          150, 200, 300, 525, 160, 200, 220, 165, 403, 240, 200, 489])
range_in_KM_norwegian_winter = np.array([          100, 125, 200, 453, 80, 125, 150, 80, 311, 160, 120, 435])







