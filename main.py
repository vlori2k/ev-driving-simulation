from create_dictionary_and_CSV import *
from top_10_EVs_norway import *
from generate_graphs_and_results import *

range_dictionary_winter   = create_dictionary_of_ranges(EV_models, range_in_KM_norwegian_winter)
range_dictionary_summer   = create_dictionary_of_ranges(EV_models, range_in_KM_norwegian_summer)

percent_dictionary_winter = make_dictionary_percent(distances, range_dictionary_winter)
percent_dictionary_summer = make_dictionary_percent(distances, range_dictionary_summer)

for models in EV_models:
    write_a_CSV_file('remainingRange_' + models, range_dictionary_winter[models], districts, "remaining_ranges_Winter")
    write_a_CSV_file('SOC_' + models, percent_dictionary_winter[models], districts, "SOC_CSV_Winter")

    write_a_CSV_file('remainingRange_' + models, range_dictionary_summer[models], districts, "remaining_ranges_Summer")
    write_a_CSV_file('SOC_' + models, percent_dictionary_summer[models], districts, "SOC_CSV_Summer")

# pick stations that you want the EV`s to pass through on the way to end-point
from_tangerud_to_fornebu = ['Fornebu',  'Lysaker E18/','Skøyen','Frogner', 'Oslo city', 'Old-Oslo', 'Helsfyr', 'Ulven/E6/Ring3', 'Alna' ,'Karihaugen E6/163/159/', 'Tangerud Interchange /E6/163/']

allModels = EV_models

car = 'BMW i3 22kWh'
car2 = 'Nissan Leaf 24 kWh pack'
car3 = 'Tesla Model S 90(D)'
car4 = 'KIA SOUL'
car5 = 'Renault ZOE R90 Z.E 40'
car6 = 'Tesla model X 90D'


percentLog = calculate_remaining_SOC(route=from_tangerud_to_fornebu,
                                     model=car,
                                     data=distances,
                                     ranges=range_in_KM_norwegian_winter,
                                     districts=districts,
                                     EVs=EV_models,
                                     distance_already_travelled=0)


percentLog2 = calculate_remaining_SOC(route=from_tangerud_to_fornebu,
                                      model=car2,
                                      data=distances,
                                      ranges=range_in_KM_norwegian_winter,
                                      districts=districts,
                                      EVs=EV_models,
                                      distance_already_travelled=0)


plot_result_to_a_graph([percentLog, percentLog2], allModels, overRideNameList=[car, car2], distances=from_tangerud_to_fornebu)